import React from 'react';
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";

function Layout({children, title, description}) {
    return (
        <>
            <Helmet titleTemplate={`%s | nurik`}>
                <title>{title}</title>
                <meta name="description" content={description}/>
                <meta name="keywords" content="react, meta tags, seo"/>
                <meta name="author" content="rakhmatov1ch"/>
            </Helmet>
            <ul className="flex justify-center gap-5 py-4 font-semibold text-[20px]">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
                <li>
                    <Link to="/posts">Posts</Link>
                </li>
            </ul>
            <div className="h-screen w-full flex flex-col overflow-y-scroll items-center justify-center">
                {children}
            </div>
        </>
    );
}

export default Layout;
