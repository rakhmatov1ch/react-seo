import Layout from "../components/Layout.jsx";

function Home() {
    return (
        <Layout title="Home Page" description="This is home page for Nurik's project">
            <h1 className="text-[60px] font-semibold">Home Page</h1>
        </Layout>
    );
}

export default Home;
