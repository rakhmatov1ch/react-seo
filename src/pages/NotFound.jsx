import Layout from "../components/Layout.jsx";

function NotFound() {
    return (
        <Layout title="Page Not Found" description="Nothing is found in Nurik's project">
            <h1 className="text-[40px] font-semibold">Oops..</h1>
            <h1 className="text-[40px] font-semibold">Page not found :(</h1>
            <h1 className="text-[60px] font-semibold">404</h1>
        </Layout>
    );
}

export default NotFound;
