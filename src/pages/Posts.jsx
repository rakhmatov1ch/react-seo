import Layout from "../components/Layout.jsx";
import {useState} from "react";

const dummyData = [
    {id: 1, title: 'Title here some more text here should be like Lorem Ipsum...'},
    {id: 2, title: 'Title here some more text here should be like Lorem Ipsum...'},
    {id: 3, title: 'Title here some more text here should be like Lorem Ipsum...'},
    {id: 4, title: 'Title here some more text here should be like Lorem Ipsum...'},
    {id: 5, title: 'Title here some more text here should be like Lorem Ipsum...'},
]

function Posts() {
    return (
        <Layout title="Posts page" description="Some wired posts page in Nurik's project">
            <h1 className="text-[60px] font-semibold">Posts</h1>
            {dummyData.map(item => (
                <div key={item.id} className="flex gap-2 overflow-hidden">
                    <p>№ {item.id}</p>
                    <p>{item.title}</p>
                </div>
            ))}
        </Layout>
    );
}

export default Posts;
